(ns ex-1.core
  (:use [ex-1.use])
  (:require [ex-1.req :refer [working]]
            [ex-1.people :as pple]))

(defn main []
  (createnamespace)
  (working)
  ;(unworking) ;Exception in thread "main" java.lang.RuntimeException: Unable to resolve symbol:  unworking in this context, compiling:(ex_1/core.clj:8:3)
  (pple/showperson))
