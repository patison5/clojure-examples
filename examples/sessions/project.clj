(defproject sessions "0.1.0-SNAPSHOT"
  :description "Clojure sessions example"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring "1.4.0"]
                 [compojure "1.4.0"]
                 [hiccup "1.0.5"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler sessions.core/app})



; :main ^:skip-aot sessions.core
;   :target-path "target/%s"
;   :profiles {:uberjar {:aot :all}}