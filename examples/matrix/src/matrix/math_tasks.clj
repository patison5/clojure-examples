(ns matrix.math-tasks)


; Структyра map
(def person [:name "Fedor"  :nickname "patison5" :pass 12345])

(defn showperson
  "выводим структуру map"
  []
  (println "My profile is ")
  (println person)
  (println ""))
; Структyра map




; числа фиббоначи
(defn fibo
  "находим число фиббоначи"
  ([] (concat [1 1] (fibo 1 1)))
  ([a b]
   (let [n (+ a b)]
     (lazy-seq (cons n (fibo b n))))))

(defn showfibo
   "выводим число фиббоначи"
   []

   (def many-fibs (take 100 (fibo)))
   (println (str "Fibbonachi number: " (nth many-fibs 5)))

   (println "time lost on searching the number of Fibbonachi")
   (time (def fi (nth many-fibs 5)))
   (println ""))
; числа фиббоначи




;использование стандартных структуры данных.
(defn vrange1 [n]
  (loop [i 0
         v []]
    ;(println v)
    (if (< i n)
      (recur (inc i) (conj v i))
      v)))
;использование стандартных структуры данных.

;использование переходных структуры данных.
(defn vrange2 [n]
  (loop [i 0
         v (transient [])]
    ;(println v)
    (if (< i n)
      (recur (inc i) (conj! v i))
      (persistent! v))))


(defn showvrange
  []
  (println "time lost for making standart data structure")
  (time (def v2 (vrange1 100000)))

  (println "")
  (println "time lost for making crossing data structure")
  (time (def v2 (vrange2 100000)))
  (println ""))
;использование переходных структуры данных.



;проверка на число или текст
(defn f1 [n]
  "проверяем число или текст"
  (cond
    (number? n) "number"
    (string? n) "string"
    :else "undefinded"))

(defn showf1
  []
  (print "type of \"patison5\" is -- ")
  (println  (f1 "patison5"))
  (print "type of 1203023112 is -- ")
  (println  (f1 1203023112)))
;проверка на число или текст




;массив из 15 элементов
(defn level1 [n]
  (println "")
  (loop [i 1
         v []]
    ;(println v)
    (if (<= i n)
      (recur (inc i) (conj v i)) (println v)))

  (println ""))
;массив из 15 элементов

; для отладки
(defn exp
  "experimental operations"
  []
  (def k 10)
  (def g 15)
  (print "g = ")
  (println g)
  (print "k = ")
  (println k)
  (println (inc g)))
;для отладки


;чтение строки
(defn restroke
  []
  (def stroke (read-line))
  (println stroke))
;чтение строки

(defn readmas
  [n m []]
  (loop [i 1 m []]
    (if (<= i n)
      (recur (inc i) (conj m (read-line))))))

(defn writemas
  [n m []]
  (loop [i 1 m []]
    (if (<= i n)
      (recur (inc i) (println m)))))


;факториал числа
(defn factorial
  [n]
  (println "")
  (loop [cnt n acc 1]
    (if (zero? cnt)
      (println (str "Factorial of '" n "' is " acc)) ;выводит факториал числа
      (recur (dec cnt) (* acc cnt)))))

(defn factorial2
  [n]
  (println (str "Factorial of '" n "' is " (reduce * (range 1 (inc n))))))

;(println (time (factorial 4)))
;(println (time (factorial2 4)))
;факториал числа


; вовод номера символа из таблицы ASCII
(defn asci
  "соответвие с таблицой ASCII"
  [a]
  (println "")
  (println (str "number of '" a "' in ASCII table: " (int a))))
; вовод номера символа из таблицы ASCII