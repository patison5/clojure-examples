(ns matrix.database)

(def db {:classname "com.mysql.jdbc.Driver"
         :subprotocol "mysql"
         :subname "//localhost:3306/athenedb"
         :user "root"
         :password ""})




;(ns athene.installation
;  (:require [clojure.java.jdbc :as sql]
;            [compojure.core :refer :all]
;            [athene.database :refer [db]]
;            [hiccup.core :refer :all]
;            [hiccup.page :refer :all]
;            [ring.util.anti-forgery :refer [anti-forgery-field]]
;            [clojure.edn :as edn]
;            [athene.roles :refer :all]
;            [clojure.java.io :as io])
;  (:use clojure.java.io))

;(ns athene.core
;  (:require
;    [compojure.core :refer :all]
;    [compojure.handler :as handler]
;    [compojure.route :as route]
;    [athene.layout :as layout]
;    [ring.middleware.defaults :refer :all]
;    [athene.posts :refer :all]
;    [athene.installation :refer :all]
;    [athene.admin.general :refer [admin-routes]]
;    [athene.roles :refer :all])
;  (:gen-class))


;(defroutes athene-routes
;           (GET "/" []
;                (layout/index-page (get-posts)))
;           (route/resources "/"))
;(def app
;  (wrap-defaults (routes athene-routes
;                         installation-routes
;                         admin-routes) site-defaults))





;roles
(ns matrix.roles
  (:require
    [clojure.java.jdbc :as sql]
    [athene.database :refer [db]]))

(defn insert-into-tables []
  (sql/insert! db :roles
               {:role_id "1" :role_name "admin"}
               {:role_id "2" :role_name "user"}
               {:role_id "3" :role_name "corrector"})

  (sql/insert! db :users
               {:user_name "patison5" :user_role "1" :password "admin_root"}))

(defn new-user
  "Добавляем пользователя в базу данных"
  [sname role password]l
  (sql/insert! db :users
               {:user_name sname :user_role role :password password}))

(defn start-join-db
  []
  (sql/query db "SELECT * FROM users
    LEFT JOIN roles ON (users.user_role = roles.role_id);"))



(defn create-roles
  []
  "Создание ролей"
  (insert-into-tables)
  (new-user "Roma_Beorn" "2" "Root")
  (start-join-db))