(ns matrix.Evklid)

(defn nod [a b]
  (println "")
  (loop [i 1
         v []]
    ;(println v)
    (if (or (> a 0) (> b 0))
      (recur
        (fn
          []
          (inc i)
          (if (> a b)
            (fn
              []
              (def max (- a b))
              (- a b))
            (fn
              []
              (def max (- b a))
              (- b a))))
        (conj v max))

      (println v)))

  (println ""))